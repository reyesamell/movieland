//
//  Colors.swift
//  Movieland
//
//  Created by Mac User on 9/17/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import UIKit

extension UIColor{
    
    class var white_lightPurple: UIColor {
        return UIColor(red: 240 / 255.0, green: 240 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    

    
    class var awesome: UIColor {
        return UIColor(red: 255 / 255.0, green: 22 / 255.0, blue: 84.0 / 255.0, alpha: 1.0)
    }
}
