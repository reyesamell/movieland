//
//  SearchVCView.swift
//  Movieland
//
//  Created by Mac User on 9/16/17.
//  Copyright © 2017 Hugo. All rights reserved.
//
import SnapKit
import UIKit

class SearchVCView: UIView {
    
    let inputText : UITextField = {
        let text = UITextField()
        text.placeholder = "Search a movie"
        return text
    }()

    let searchButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "search"), for: .normal)
        return button
    }()
    
    lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 150, height: 250)
        return layout
    }()
    
    lazy var collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    let lineUpInput : UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.black
        return line
    }()
    
    
    let lineDownInput : UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.black
        return line
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init() {
        super.init(frame: .zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func setupLayout(){
        self.addSubview(self.searchButton)
        self.searchButton.snp.makeConstraints { (make) in
            make.right.equalTo(self)
            make.width.equalTo(self).multipliedBy(0.2)
            make.top.equalTo(60)
            make.height.equalTo(50)
        }
        
        self.addSubview(self.inputText)
        self.inputText.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.left.equalTo(self).offset(20)
            make.right.equalTo(self.searchButton.snp.left)
            make.height.centerY.equalTo(self.searchButton)
        }

        self.addSubview(self.lineUpInput)
        self.lineUpInput.snp.makeConstraints { (make) in
            make.top.equalTo(self.inputText)
            make.width.centerX.equalTo(self)
            make.height.equalTo(1)
        }

        self.addSubview(self.lineDownInput)
        self.lineDownInput.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.inputText)
            make.height.width.centerX.equalTo(self.lineUpInput)
            
        }
        
        self.layoutIfNeeded()
        let itemWidth: CGFloat = self.frame.width
        self.collectionViewLayout.itemSize = CGSize(width: itemWidth, height: 900)
        self.collectionView.collectionViewLayout = self.collectionViewLayout
        
        self.addSubview(collectionView)
        self.collectionView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(110)
            make.center.width.equalTo(self)
            make.bottom.equalTo(-40)
        }
        self.collectionView.backgroundColor = UIColor.white
    }
    
    
}

