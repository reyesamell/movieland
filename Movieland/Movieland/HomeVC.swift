//
//  ViewController.swift
//  Movieland
//
//  Created by Mac User on 8/1/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import Kingfisher
import Moya
import Alamofire
import SwiftyJSON

class HomeVC: UIViewController, UICollectionViewDelegate {
    
    // View
    let mainView: HomeView = HomeView()
    // Model
    var homeModel: HomeModel = HomeModel()
    // Variables asociated to collection view:
    var currentPage: Int = 0
    let provider = MoyaProvider<MyService>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.genresCollectionView.register(
            GenderCollectionCell.self,
            forCellWithReuseIdentifier: "category")
        
        self.mainView.collectionView.register(
            MovieCollectionCell.self,
            forCellWithReuseIdentifier: "normal")
        
        self.mainView.genresCollectionView.delegate = self
        
        self.view.addSubview(self.mainView)
        self.mainView.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.width.height.equalTo(self.view)
        }
        
        self.mainView.addingCollectionView()
        self.bindViewElements()
        self.getGenres()
        self.mainView.backgroundColor = UIColor.white
    }


    override func viewDidAppear(_ animated: Bool) {
        self.offsetProgrammatically()
        self.view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
    }

    
    func getGenres(){
        /*
         This method gets all the genres available from the server
         */
        provider.request(.getGenres) { (result) in
            switch result {
            case let .success(moyaResponse):
                
                self.homeModel.genres =  Genres(data: moyaResponse.data).genres
                
            case let .failure(error):
                print(error)
                break
            }
        }
        
        
    }
    
    func bindViewElements(){
        /*
         This method makes the binding between the view elements (collection views) and the observable model variables
         */
        self.homeModel.movies.asObservable().bind(to: self.mainView.collectionView.rx.items(cellIdentifier: "normal")){ row,Searchplace, cell in
            if let movieCollectionCell = cell as? MovieCollectionCell{
                let movie: Movie = self.homeModel.movies.value[row]
                movieCollectionCell.setLayouts()
                movieCollectionCell.title.text = movie.title
                movieCollectionCell.moviePic.kf.setImage(with: URL(string: "https://image.tmdb.org/t/p/w320/"  + movie.poster_path))
            
                movieCollectionCell.popularityValue.text = "\(String(format: "%.2f", movie.popularity))"
                movieCollectionCell.vote_countValue.text = "\(movie.vote_count)"
                movieCollectionCell.overviewValue.text = movie.overview
                movieCollectionCell.genreValue.text = HomeModel.getGenresStringNames(genres: self.homeModel.genres, ids: self.homeModel.movies.value[row].genre_ids)
            }
            }.addDisposableTo(self.homeModel.disposeBag)
        
        
        self.homeModel.categories.asObservable().bind(to: self.mainView.genresCollectionView.rx.items(cellIdentifier: "category")){ row,_, cell in
            if let genreCollectionCell = cell as? GenderCollectionCell{
                genreCollectionCell.setLayouts()
                genreCollectionCell.title.text = self.homeModel.categories.value[row]
            }
            }.addDisposableTo(self.homeModel.disposeBag)
        
    }
    
    func getMovieList(currentPage: Int){
        var category: String = ""
        
        switch  currentPage{
        case 0:
            // Upcoming
            category = "release_date.desc"
        case 1:
            // Popular
            category = "popularity.desc"
        case 2:
            // Top rated
            category = "vote_average.desc"
        default:
            category = "release_date.desc"
        }
        
        guard Reachability.isConnectedToNetwork() else{
            self.showAlert(title: "Error", message: "Please, check your Internet connection")
            return
        }
        self.provider.request(.movieList(parameters:  ["api_key": "65e58928ce61c1a1cd62bf76abc1eaa2", "sort_by":category])) { (result) in
            switch result {
                case let .success(moyaResponse):
        
                    self.homeModel.movies.value = Movies(data: moyaResponse.data).results
                
            case .failure(_):
                self.showAlert(title: "Error", message: "Something went wrong, please try again")
                break
                }
            }
        }
    
    
}
