//
//  HomeModel.swift
//  Movieland
//
//  Created by Mac User on 8/4/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON
import UIKit

struct HomeModel{
    
    // Mark: Observables
    var movies: Variable<[Movie]> = Variable(
        [])
    
    var categories: Variable<[String]> = Variable(["Upcoming","Popular","Top Rated", "Upcoming", "Popular"])
    // Need for delete rx object instances
    let disposeBag = DisposeBag()
    
    var genres: [Genre] = []
    
    static func extractNameFromGenre(genres: [Genre], id: Int) -> String{
        guard genres.count > 0 else{return ""}
        
        for genre in genres{
            if genre.id == id{return genre.name}
        }
        return ""
    }
    
    static func getGenresStringNames(genres: [Genre], ids: [Int])-> String{
        var names : [String] = []
        for id in ids{
            let name = HomeModel.extractNameFromGenre(genres: genres, id: id)
            if  name != "" {
                names.append(name)
            }
        }
        return names.joined(separator: ",")
    }
}



