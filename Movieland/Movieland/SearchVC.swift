//
//  SearchVC.swift
//  Movieland
//
//  Created by Mac User on 8/5/17.
//  Copyright © 2017 Hugo. All rights reserved.


import UIKit
import SnapKit
import Moya
import RxCocoa
import Kingfisher

class SearchVC: UIViewController, UICollectionViewDelegate, UITextFieldDelegate {
    let mainView: SearchVCView = SearchVCView()
    let homeModel: HomeModel = HomeModel()
    let provider = MoyaProvider<MyService>()
    
    override func viewDidLoad() {
        self.view.addSubview(self.mainView)
        self.mainView.snp.makeConstraints { (make) in
            make.center.height.width.equalTo(self.view)
        }
        
        self.mainView.collectionView.register(
            MovieCollectionCell.self,
            forCellWithReuseIdentifier: "normal")
        
        self.mainView.setupLayout()
        self.mainView.inputText.delegate = self
        self.mainView.searchButton.addTarget(self, action: #selector(self.searchAction), for: .touchUpInside)
        
        self.view.backgroundColor = UIColor.white
        self.bindViewElements()
    }
    
    func searchAction(){
        guard Reachability.isConnectedToNetwork() else{
            self.showAlert(title: "Error", message: "Please, check your Internet connection")
            return
        }
        
        self.provider.request(.querySearch(parameters:  ["api_key": "65e58928ce61c1a1cd62bf76abc1eaa2", "query": self.mainView.inputText.text!])) { (result) in
            switch result {
            case let .success(moyaResponse):
                
                self.homeModel.movies.value = Movies(data: moyaResponse.data).results
                
            case .failure(_):
                self.showAlert(title: "Error", message: "Something went wrong, please try again")
                break
            }
        
        }
        
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        self.inputView?.resignFirstResponder()
    }

    func unregisterKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // This function hide the keyboard when press enter
        textField.resignFirstResponder()  //if desired
        self.searchAction()
        return true
    }

    func bindViewElements(){
        /*
         This method makes the binding between the view elements (collection views) and the observable model variables
         */
        self.homeModel.movies.asObservable().bind(to: self.mainView.collectionView.rx.items(cellIdentifier: "normal")){ row,Searchplace, cell in
            if let movieCollectionCell = cell as? MovieCollectionCell{
                let movie: Movie = self.homeModel.movies.value[row]
                movieCollectionCell.setLayouts()
                movieCollectionCell.title.text = movie.title
                movieCollectionCell.moviePic.kf.setImage(with: URL(string: "https://image.tmdb.org/t/p/w320/"  + movie.poster_path))
                
                movieCollectionCell.popularityValue.text = "\(String(format: "%.2f", movie.popularity))"
                movieCollectionCell.vote_countValue.text = "\(movie.vote_count)"
                movieCollectionCell.overviewValue.text = movie.overview
                movieCollectionCell.genreValue.text = HomeModel.getGenresStringNames(genres: self.homeModel.genres, ids: self.homeModel.movies.value[row].genre_ids)
            }
            }.addDisposableTo(self.homeModel.disposeBag)
                
    }
    
    
}
