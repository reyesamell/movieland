//
//  MovieCollectionCell.swift
//  Movieland
//
//  Created by Mac User on 8/5/17.
//  Copyright © 2017 Hugo. All rights reserved.

import UIKit
import SnapKit

class MovieCollectionCell: UICollectionViewCell{
    
    let moviePic: UIImageView = {
       let image = UIImageView()
        image.backgroundColor = UIColor.white
        return image
    }()

    let title : CustomLabel = CustomLabel(
        text: "",
        size: 24,
        aligment: .center)
    
    let popularityTitle : CustomLabel = CustomLabel(
        text: "Popularity",
        size: 22, aligment: .center)
    
    let popularityValue : CustomLabel = CustomLabel(
        text: "",
        size: 18)
    
    let vote_averageTitle : CustomLabel = CustomLabel(
        text: "Vote average",
        size: 22, aligment: .center)
    
    let vote_averageValue : CustomLabel = CustomLabel(
        text: "",
        size: 18)
    
    let original_languageTitle : CustomLabel = CustomLabel(
        text: "Original language",
        size: 22, aligment: .center)
    
    let original_languageValue : CustomLabel = CustomLabel(
        text: "",
        size: 18)
    
    let release_dateTitle : CustomLabel = CustomLabel(
        text: "Release date",
        size: 22, aligment: .center)
    
    let release_dateValue : CustomLabel = CustomLabel(
        text: "",
        size: 18)
    
    let vote_countTitle : CustomLabel = CustomLabel(
        text: "Vote count",
        size: 22,
        aligment: .center)
    
    let vote_countValue : CustomLabel = CustomLabel(
        text: "",
        size: 18)

    let overviewTitle : CustomLabel = CustomLabel(
        text: "Overview",
        size: 22, aligment: .center)
    
    let overviewValue : CustomLabel = CustomLabel(
        text: "",
        size: 18)

    let genreTitle : CustomLabel = CustomLabel(
        text: "Genre",
        size: 22, aligment: .center)
    
    let genreValue : CustomLabel = CustomLabel(
        text: "",
        size: 18)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setLayouts(){
        
        self.addSubview(self.title)
        self.title.snp.makeConstraints { (make) in
            make.top.equalTo(self)
            make.height.equalTo(40)
            make.width.centerX.equalTo(self)
        }
        
        self.addSubview(self.moviePic)
        self.moviePic.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(40)
            make.centerX.equalTo(self)
            make.width.equalTo(self).multipliedBy(0.8)
            make.height.equalTo(self.snp.width).multipliedBy(1.2)
        }
        
        self.addSubview(self.popularityTitle)
        self.popularityTitle.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(40)
            make.width.equalTo(self).multipliedBy(0.8)
            make.top.equalTo(self.moviePic.snp.bottom).offset(10)
            make.height.equalTo(30)
        }
       
        self.addSubview(self.popularityValue)
        self.popularityValue.snp.makeConstraints { (make) in
            make.left.width.height.equalTo(self.popularityTitle)
            make.top.equalTo(self.popularityTitle.snp.bottom)
        }
        
        self.addSubview(self.vote_countTitle)
        self.vote_countTitle.snp.makeConstraints { (make) in
            make.left.width.height.equalTo(self.popularityValue)
            make.top.equalTo(self.popularityValue.snp.bottom)
        }
        
        self.addSubview(self.vote_countValue)
        self.vote_countValue.snp.makeConstraints { (make) in
            make.left.width.height.equalTo(self.vote_countTitle)
            make.top.equalTo(self.vote_countTitle.snp.bottom)
        }
        
        self.addSubview(self.overviewTitle)
        self.overviewTitle.snp.makeConstraints { (make) in
            make.left.width.height.equalTo(self.vote_countValue)
            make.top.equalTo(self.vote_countValue.snp.bottom)
        }
        
        
        self.addSubview(self.overviewValue)
        self.overviewValue.snp.makeConstraints { (make) in
            make.left.width.equalTo(self.overviewTitle)
            make.top.equalTo(self.overviewTitle.snp.bottom)
            make.height.equalTo(150)
        }
        
        self.overviewValue.lineBreakMode = NSLineBreakMode.byCharWrapping
        self.overviewValue.numberOfLines = 0
        
        self.addSubview(self.genreTitle)
        self.genreTitle.snp.makeConstraints { (make) in
            make.left.width.height.equalTo(self.vote_countValue)
            make.top.equalTo(self.overviewValue.snp.bottom)
        }
        
        self.addSubview(self.genreValue)
        self.genreValue.snp.makeConstraints { (make) in
            make.left.width.height.equalTo(self.genreTitle)
            make.top.equalTo(self.genreTitle.snp.bottom)
        }
        
        self.backgroundColor = UIColor.white_lightPurple
        self.layer.cornerRadius = 10
    }

}
