//
//  UIViewController+Utilities.swift
//  Movieland
//
//  Created by Mac User on 9/20/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import UIKit

extension UIViewController{
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
            
        })
        self.present(alert, animated: true, completion: nil)
    }
    
}
