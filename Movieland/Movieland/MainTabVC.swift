//
//  MainTabVC.swift
//  Movieland
//
//  Created by Mac User on 8/5/17.
//  Copyright © 2017 Hugo. All rights reserved.

import UIKit

class MainTabVC: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    func createControllerAndItem(viewController: UIViewController, imageNormal: UIImage, imageSelected: UIImage, tag: Int) -> UIViewController {
        let tabBarItem = UITabBarItem(title: nil, image: imageNormal.withRenderingMode(.alwaysOriginal), selectedImage: imageSelected.withRenderingMode(.alwaysOriginal))
        tabBarItem.tag = tag
        viewController.tabBarItem = tabBarItem
        viewController.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0)
        return viewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let homeVC = self.createControllerAndItem(viewController: HomeVC(), imageNormal:#imageLiteral(resourceName: "home"), imageSelected:#imageLiteral(resourceName: "ic_home"), tag: 0)
        let searchVC = self.createControllerAndItem(viewController: SearchVC(), imageNormal:#imageLiteral(resourceName: "search"), imageSelected:#imageLiteral(resourceName: "ic_search"), tag: 1)
        
        self.viewControllers = [homeVC, searchVC]
    }
    
}
