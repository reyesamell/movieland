//
//  GenderCollectionCell.swift
//  Movieland
//
//  Created by Mac User on 8/24/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import UIKit
import SnapKit

class GenderCollectionCell: UICollectionViewCell{
    
    let title: UILabel = {
        let label: UILabel = UILabel()
        label.font.withSize(18)
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setLayouts(){
        self.backgroundColor = UIColor.white
        
        self.addSubview(self.title)
        self.title.snp.makeConstraints { (make) in
            make.center.height.width.equalTo(self)
        }
    }
    
}

