//
//  NavigationController.swift
//  Movieland
//
//  Created by Mac User on 8/2/17.
//  Copyright © 2017 Hugo. All rights reserved.

import UIKit

class NavigationController: UINavigationController, UIViewControllerTransitioningDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.isTranslucent = false
        self.navigationBar.backgroundColor = .clear
        self.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "1px transparent"),  for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
    
        
    }
    
    
    
}
