//
//  Genres.swift
//  Movieland
//
//  Created by Mac User on 9/16/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import UIKit
import SwiftyJSON



struct Genre {
    var id: Int!
    var name: String = ""
    
    init(id: Int, name: String){
        self.id = id
        self.name = name
    }
}


struct Genres {
    var genres: [Genre] = []
    
    init(data: Data){
        // From data we read all information to create the genre list
        let json: JSON = JSON(data: data)
        if let rawGenres = json["genres"].arrayObject{
            for element in rawGenres{
                if let jsonElement = element as? NSDictionary{
                    if let name = jsonElement["name"] as? String{
                        if let id = jsonElement["id"] as? Int{
                            self.genres.append(Genre(id: id, name: name))
                        } // if let id
                    } // if let name
                }
            } // End of for loop
        }
        
        
        
    } // End of init method
}

