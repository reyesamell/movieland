//
//  HomeVC+CollectionFunctions.swift
//  Movieland
//
//  Created by Mac User on 9/10/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import UIKit

extension HomeVC{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /*
         In this method the current offset if checked in order to move offset if needed (i.e. when scroll content is near to its end, the offset is changed to start again)
         */
        guard scrollView == self.mainView.genresCollectionView else{return}
        
        guard let layout = self.mainView.genresCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else {return}
        
        let size = layout.itemSize
        let pageSide =  size.width
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        
        let center = self.view.center.x
        let offsetNewCurrentPage = Int(floor(( offset + center) / pageSide))
        guard self.currentPage == offsetNewCurrentPage else{
            self.currentPage = offsetNewCurrentPage
            self.offsetProgrammatically()
            self.mainView.genresCollectionView.endInteractiveMovement()
            return
        }
        
        
        // NominalRightOffset is the maximun scroll based on the content size and screen size
        
        let nominalRightOffset = self.mainView.genresCollectionView.contentSize.width - self.view.frame.size.width

        if offset > nominalRightOffset{
            self.mainView.genresCollectionView.setContentOffset(CGPoint(x: -(self.view.frame.size.width - (pageSide * 2)), y:0), animated: false)
            self.offsetProgrammatically()
        }
        if offset < 0{
            //self.mainView.genresCollectionView.setContentOffset(CGPoint(x: pageSide * CGFloat(self.homeModel.genres.count - 2 ), y:0), animated: false)
            self.mainView.genresCollectionView.setContentOffset(CGPoint(x: pageSide * CGFloat(self.homeModel.categories.value.count - 2 ), y:0), animated: false)
            
            self.offsetProgrammatically()
            
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        /* This method is called when collection view stop after a gesture action
         */
        
        guard let layout = self.mainView.genresCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else {return}
        
        let pageSide = (layout.scrollDirection == .horizontal) ? layout.itemSize.width : layout.itemSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        
        self.currentPage = Int(floor(( offset + self.view.center.x) / pageSide))
        self.offsetProgrammatically()
        
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            scrollViewDidEndDecelerating(scrollView)
        }
    }
    
    func offsetProgrammatically(){
        /*
         This method programmatically offset the current page or current colllection cell to the center
         */
        guard let pageSize = self.mainView.genresCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else{
            return}
        
        let x: CGFloat = CGFloat(self.currentPage + 1) * pageSize.itemSize.width - (pageSize.itemSize.width/2) - self.view.frame.size.width/2
        self.mainView.genresCollectionView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
        
        self.mainView.genresCollectionView.endInteractiveMovement()
        self.getMovieList(currentPage: self.currentPage)
    }
    
    
    
}
