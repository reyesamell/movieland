//
//  MyService.swift
//  Movieland
//
//  Created by Mac User on 8/6/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import Foundation
import Moya

enum MyService {
    case getGenres
    case movieList(parameters: [String: String])
    case querySearch(parameters: [String: String])
}

// MARK: - TargetType Protocol Implementation
extension MyService: TargetType {
    var baseString : String { return "https://api.themoviedb.org" }
    var baseURL: URL {
        switch self {
        case .getGenres:
            return URL(string: baseString + "/3/genre/movie/list?")!
        case .movieList:
            return URL(string: baseString + "/3/discover/movie?")!
        case .querySearch:
            return URL(string: baseString + "/3/search/movie?")!
        }
        
    }
    
    var path: String {return ""}
    
    var method: Moya.Method {
        switch self {
        case .getGenres, .movieList, .querySearch:
            return .get
        
        }
        
    }
    var parameters: [String: Any]? {
        switch self {
        case .getGenres:
            return ["api_key": "65e58928ce61c1a1cd62bf76abc1eaa2"]
        case .movieList(let params), .querySearch(let params):
            return params
        }

    }
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getGenres, .movieList:
            return URLEncoding.default
        case .querySearch:
            return URLEncoding.queryString
        }
    }
    
    var sampleData: Data {
        switch self {
        case .getGenres, .movieList, .querySearch:
            return "".utf8Encoded
        }
    }
    
    var task: Task {
        switch self {
        case .getGenres, .movieList, .querySearch:
            return .request
        }
    }
    
    
    var headers: [String: String]? {
        return [
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2NWU1ODkyOGNlNjFjMWExY2Q2MmJmNzZhYmMxZWFhMiIsInN1YiI6IjU4YzllZTIwOTI1MTQxNWRkNjAwMTlhYSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.sQcTtTO2K8krtDU_XPKlcj3qTR_01UsDdbQ-5SKYz4Q",
            "Content-Type": "application/json"
        ]
    }
    
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}
