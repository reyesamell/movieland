//
//  HomeView.swift
//  Movieland
//
//  Created by Mac User on 8/2/17.
//  Copyright © 2017 Hugo. All rights reserved.
//

import UIKit

class HomeView: UIView {

    lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 150, height: 250)
        return layout
    }()
    
    lazy var genreCollectionViewLayout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 1, height: 1)
        return layout
    }()
    
    lazy var collectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    lazy var genresCollectionView: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

    let lineView : UIView = {
        let line = UIView()
        line.backgroundColor = .awesome
        return line
    }()
    
    let lineUpInput : UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.black
        return line
    }()

    let lineDownInput : UIView = {
        let line = UIView()
        line.backgroundColor = UIColor.black
        return line
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init() {
        super.init(frame: .zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func addingCollectionView(){
        
        self.layoutIfNeeded()
        let itemWidth: CGFloat = self.frame.width
        self.collectionViewLayout.itemSize = CGSize(width: itemWidth, height: 900)
        self.collectionView.collectionViewLayout = self.collectionViewLayout
        
        self.addSubview(collectionView)
        self.collectionView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(110)
            make.center.width.equalTo(self)
            make.bottom.equalTo(-40)
        }
        self.collectionView.backgroundColor = UIColor.white
        
        let genreCollectionHeight : CGFloat = 50
        self.addSubview(self.genresCollectionView)
        self.genresCollectionView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.top.equalTo(self).offset(20)
            make.height.equalTo(genreCollectionHeight + 40)
        }
        self.genresCollectionView.showsHorizontalScrollIndicator = false
        self.genresCollectionView.layoutIfNeeded()
        self.genreCollectionViewLayout.itemSize = CGSize(width: 200, height: genreCollectionHeight)
        self.genresCollectionView.collectionViewLayout = self.genreCollectionViewLayout
        

        self.genresCollectionView.backgroundColor = UIColor.white
        
        self.addSubview(self.lineUpInput)
        self.lineUpInput.snp.makeConstraints { (make) in
            make.width.centerX.equalTo(self.genresCollectionView)
            make.top.equalTo(60)
            make.height.equalTo(1)
        }
        
        self.addSubview(self.lineDownInput)
        self.lineDownInput.snp.makeConstraints { (make) in
            make.width.height.centerX.equalTo(self.lineUpInput)
            make.bottom.equalTo(self.genresCollectionView.snp.bottom)
        }
        
        self.addSubview(self.lineView)
        self.lineView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self.genresCollectionView.snp.bottom)
            make.height.equalTo(3)
            make.width.equalTo(self.snp.width).multipliedBy(0.5)
        }
    }
    
    
}
